﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField] Rigidbody2D rigidBody = null;
    [SerializeField] float speed = 2.0f;
    [SerializeField] float jumpForce = 10.0f;
    [SerializeField] float coinHeight = 0.15f;
    [SerializeField] CoinBullet coinBulletPrefab;
    [SerializeField] ParticleSystem explosionPrefab;

    public Action GameOverAction = delegate { };

    public bool IsLookToRight { get { return _isLookToRight; } }

    private bool isGrounded = true;
    private bool _isLookToRight = true;
    private bool isColliding = false;
    private float shootTime = 0.0f;
    private bool isKilled = false;
    private Vector3 startPosition;

    private CoinManager coinManager;
    private PlayerSoundManager soundManager;
    private GameHandler gameHandler;

    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;

    public void Restart() {
        coinManager.Restart();
        transform.position = startPosition;
        isKilled = false;
    }

    private void Awake() {
        gameHandler = FindObjectOfType<GameHandler>();
        coinManager = GetComponent<CoinManager>();
        soundManager = GetComponent<PlayerSoundManager>();
        startPosition = transform.position;
    }

    private void FixedUpdate() {
        CheckGround();
        isColliding = false;
    }

    private void Update() {
        if (isKilled) return;

        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0) {
            Move();
        } else {
            StopMove();
        }
        if (Input.GetButtonDown("Jump") && isGrounded) {
            Jump();
        }
        if (Input.GetButtonDown("Fire1")) {
            Shoot();
        }

        if (shootTime > 0.0) {
            shootTime -= Time.deltaTime;
            if (shootTime < 0)
                shootTime = 0.0f;
        }

        SmartJump();
    }

    private void Move() {
        var moveX = Input.GetAxis("Horizontal");
        rigidBody.velocity = new Vector2(moveX * speed * Time.deltaTime, rigidBody.velocity.y);
        _isLookToRight = moveX > 0.0f;
    }

    private void StopMove() {
        rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
    }

    private void Jump() {
        rigidBody.velocity = Vector2.up * jumpForce;
        soundManager.PlayJumpSound();
    }

    public void Damage() {
        if (isKilled) {
            return;
        }

        rigidBody.velocity = Vector3.zero;
        rigidBody.AddForce(transform.up * 20.0f, ForceMode2D.Impulse);
        if (coinManager.GetNumberOfCoins() > 1) {
            coinManager.RemoveCoin();
        }
        else {
            GameOver();
        }        
    }

    private void GameOver() {
        isKilled = true;
        coinManager.HideCoins();
        var explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position + new Vector3(0.0f, coinHeight, 0.0f);
        Destroy(explosion, 2.0f);
        GameOverAction();
    }

    private void Shoot() {
        if (coinManager.GetNumberOfCoins() > 1 && Mathf.Approximately(shootTime, 0.0f)) {
            shootTime = 0.5f;
            coinManager.RemoveCoin();
            var position = transform.position;
            position.y += coinHeight;
            CoinBullet coinBullet = Instantiate(coinBulletPrefab, position, Quaternion.identity);
            coinBullet.Direction = coinBullet.transform.right * (_isLookToRight ? 1.0f : -1.0f);
            soundManager.PlayShootSound();
        }
    }

    private void SmartJump() {
        if (rigidBody.velocity.y < 0) {
            rigidBody.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rigidBody.velocity.y > 0 && !Input.GetButton("Jump")) {
            rigidBody.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }

    private void CheckGround() {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f);
        isGrounded = colliders.Length > 2;
    }

    private bool OnSlope() {
        if (!isGrounded) {
            return false;
        }

        var slopeForceRayLength = 3.0f;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, coinHeight / 2 * slopeForceRayLength)) {
            if (hit.normal != Vector3.up) {
                return true;
            }
        }
        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        var coinObject = collision.gameObject.GetComponent<CoinObject>();
        if (coinObject != null) {
            if (isColliding || coinObject.IsTaken)
                return;

            coinManager.AddCoin();
            coinObject.PickupCoin();
            isColliding = true;
        }

        var gameOverColider = collision.gameObject.GetComponent<GameOverColider>();
        if (gameOverColider != null) {
            GameOver();
        }

        var finishColider = collision.gameObject.GetComponent<FinishColider>();
        if (finishColider != null) {
            gameHandler.FinishLevel();
        }
    }
}
