﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WinPanel : MonoBehaviour {
    [SerializeField] TextMeshProUGUI scoreText;

    public void UpdateWinPanel()
    {
        var scores = ScoresHandler.Instance.GetScores();
        scoreText.text = "Number of coins: " + scores;
    }
}
