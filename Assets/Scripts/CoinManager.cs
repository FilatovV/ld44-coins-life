﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    [SerializeField] GameObject coinPrefab;
    [SerializeField] float startColiderOffset = 0.25f;
    [SerializeField] float startColiserSize = 0.47f;
    [SerializeField] float coinHeight = 0.15f;
    [SerializeField] SpriteRenderer coinFace;
    [SerializeField] BoxCollider2D boxCollider = null;
    [SerializeField] Transform coinHierarchy = null;
    [SerializeField] TextMeshProUGUI coinsText;


    [SerializeField] List<Sprite> coinsFaces;

    private List<GameObject> extraCoinList = new List<GameObject>();

    private int coins = 1;

    private void Start() {
        UpdateCoinsInformation();
    }

    public void AddCoin() {
        coins++;
        AddCoinSprite();
        UpdateColaider();
        UpdateCoinsInformation();
    }

    public void Restart() {
        while (extraCoinList.Count > 0) {
            RemoveCoin();
        }
        coins = 1;
        coinHierarchy.gameObject.SetActive(true);
        UpdateCoinsInformation();
    }

    public void HideCoins() {
        coins = 0;
        UpdateCoinsInformation();
        coinHierarchy.gameObject.SetActive(false);
    }

    public void RemoveCoin() {
        if (coins > 0) {
            coins--;
        }
        if (extraCoinList.Count == 0) {
            return;
        }
        RemoveCoinSprite();
        UpdateColaider();
        UpdateCoinsInformation();
    }

    private void UpdateCoinsInformation() {
        coinsText.text = "Coins: " + coins.ToString() + "x";

        ScoresHandler.Instance.SetScore(coins);

        int index = (coins > coinsFaces.Count) ? (coinsFaces.Count - 1) : (coins - 1);
        if (index < 0)
            index = 0;
        coinFace.sprite = coinsFaces[index];
    }

    public int GetNumberOfCoins() {
        return coins;
    }

    private void UpdateColaider() {
        boxCollider.offset = new Vector2(boxCollider.offset.x, startColiderOffset + (coins - 1) * coinHeight / 2.0f);
        boxCollider.size = new Vector2(boxCollider.size.x, startColiserSize + (coins - 1) * coinHeight);
    }

    private void AddCoinSprite() {
        var coinObject = Instantiate(coinPrefab, coinHierarchy, false);
        coinObject.transform.localPosition = new Vector3(0.0f, (coins - 1) * coinHeight, 0.0f);
            //Instantiate(coinPrefab, , Quaternion.identity, coinHierarchy, );
        var spriteRenderer = coinObject.GetComponentInChildren<SpriteRenderer>();
        spriteRenderer.sortingOrder = coins - 1;

        extraCoinList.Add(coinObject);
    }

    private void RemoveCoinSprite() {
        var lastIndex = extraCoinList.Count - 1;
        if (lastIndex < 0) return;

        var coin = extraCoinList[lastIndex];
        extraCoinList.RemoveAt(lastIndex);
        Destroy(coin.gameObject);
    }
}
