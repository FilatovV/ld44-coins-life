﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolEnemy : MonoBehaviour
{
    [SerializeField] Transform leftPoint;
    [SerializeField] Transform rightPoint;

    private bool isGoingToRight = true;
    private float speed = 200.0f;
    private Rigidbody2D rigidBody;

    private void Awake() {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {        
        if (leftPoint != null && rightPoint != null) {
            UpdatePatrol();
        }        
    }

    private void UpdatePatrol() {
        if (isGoingToRight && transform.position.x >= rightPoint.position.x) {
            isGoingToRight = false;
        }
        else if (!isGoingToRight && transform.position.x <= leftPoint.position.x) {
            isGoingToRight = true;
        }

        if (isGoingToRight) {
            rigidBody.velocity = new Vector2(1 * speed * Time.deltaTime, rigidBody.velocity.y);
        }
        else {
            rigidBody.velocity = new Vector2(-1 * speed * Time.deltaTime, rigidBody.velocity.y);
        }
    }
}
