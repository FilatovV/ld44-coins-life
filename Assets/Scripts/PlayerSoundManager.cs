﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundManager : MonoBehaviour {

    [SerializeField] AudioSource playerAudioSource;
    [SerializeField] AudioClip shootAudioClip;
    [SerializeField] AudioClip jumpAudioClip;

    public void PlayShootSound() {
        playerAudioSource.PlayOneShot(shootAudioClip);
    }

    public void PlayJumpSound() {
        playerAudioSource.PlayOneShot(jumpAudioClip);
    }
}
