﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    [SerializeField] Enemy enemy;

    private void Awake() {
        var gameHandler = FindObjectOfType<GameHandler>();
        gameHandler.RestartGameAction += Restart;
    }

    public void KillEnemy() {
        enemy.gameObject.SetActive(false);
    }

    public void Restart() {
        enemy.gameObject.SetActive(true);
        enemy.Restart();
    }
}
