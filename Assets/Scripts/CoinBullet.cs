﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBullet : MonoBehaviour
{
    public Vector3 Direction { get; set; }
    private float speed = 20.0f;

    private void Start() {
        Destroy(gameObject, 3.0f);
    }

    private void Update() {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + Direction, speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        var enemy = collision.GetComponent<Enemy>();

        if (enemy != null) {
            Destroy(gameObject);
        }
    }
}
