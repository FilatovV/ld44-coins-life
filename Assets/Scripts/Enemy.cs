﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] EnemyManager enemyManager;

    private Vector3 startPosition;

    private void Awake() {
        startPosition = transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        var player = collision.GetComponent<Player>();

        if (player != null && collision is BoxCollider2D) {
            player.Damage();
        }

        var bullet = collision.GetComponent<CoinBullet>();
        if (bullet != null) {
            enemyManager.KillEnemy();
        }
    }

    public void Restart() {
        transform.position = startPosition;
    }
}
