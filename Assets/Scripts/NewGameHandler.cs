﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGameHandler : MonoBehaviour {
    void Start() {
        ScoresHandler.Instance.NewGame();
    }
}
