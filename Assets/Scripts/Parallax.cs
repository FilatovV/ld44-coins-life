﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] float backroundSize;
    [SerializeField] Camera myCamera;
    public float parallaxSpeed;

    private Transform[] layers;
    private int leftIndex;
    private int rightIndex;
    private float viewZone = 4.0f;
    private float lastCameraX;

    void Start() {
        lastCameraX = myCamera.transform.position.x;
        layers = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++) {
            layers[i] = transform.GetChild(i);
        }

        leftIndex = 0;
        rightIndex = layers.Length - 1;
    }

    private void ScrollLeft() {
        int lastRight = rightIndex;
        layers[rightIndex].localPosition = Vector3.right * (layers[leftIndex].localPosition.x - backroundSize);
        leftIndex = rightIndex;
        rightIndex--;
        if (rightIndex < 0)
            rightIndex = layers.Length - 1;
    }

    private void ScrollRight() {
        int lastLeft = leftIndex;
        layers[leftIndex].localPosition = Vector3.right * (layers[rightIndex].localPosition.x + backroundSize);
        rightIndex = leftIndex;
        leftIndex++;
        if (leftIndex == layers.Length)
            leftIndex = 0;
    }

    void Update() {
        float deltaX = myCamera.transform.position.x - lastCameraX;
        transform.position += Vector3.right * (deltaX * parallaxSpeed);
        lastCameraX = myCamera.transform.position.x;

        if (myCamera.transform.position.x < (layers[leftIndex].transform.position.x + viewZone)) {
            ScrollLeft();
        }
        else if (myCamera.transform.position.x > (layers[rightIndex].transform.position.x - viewZone)) {
            ScrollRight();
        }

        transform.position = new Vector3(transform.position.x, myCamera.transform.position.y, transform.position.z);
    }
}
