﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinObject : MonoBehaviour
{
    [SerializeField] SpriteRenderer sprite;
    [SerializeField] AudioSource audioSource;

    public bool IsTaken { get { return _isTaken; } }

    private bool _isTaken = false;

    private void Awake() {
        var gameHandler = FindObjectOfType<GameHandler>();
        gameHandler.RestartGameAction += Restart;
    }

    private void Start() {
        sprite = GetComponentInChildren<SpriteRenderer>();
        audioSource = GetComponentInChildren<AudioSource>();
    }

    public void PickupCoin() {
        _isTaken = true;
        audioSource.Play();
        sprite.gameObject.SetActive(false);
    }

    public void Restart() {
        _isTaken = false;
        sprite.gameObject.SetActive(true);
    }
}
