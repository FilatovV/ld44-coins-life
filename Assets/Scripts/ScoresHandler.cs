﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoresHandler : SingletonMonoBehaviour<ScoresHandler> {
    int scores = 1;
    int startLevelScores = 0;

    public Action<int> ScoreUpdateAction = delegate { };

    public void NewGame() {
        startLevelScores = 0;
        scores = 1;
        ScoreUpdateAction(scores);
    }

    public void SaveScore() {
        startLevelScores = scores;
        scores = startLevelScores;
    }

    public void SetScore(int scoreForLevel) {
        scores = startLevelScores + scoreForLevel;
        ScoreUpdateAction(scores);
    }

    public void UpdateScore() {
        ScoreUpdateAction(scores);
    }

    public int GetScores() {
        return scores;
    }
}
