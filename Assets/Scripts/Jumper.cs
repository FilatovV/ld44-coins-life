﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : MonoBehaviour
{
    [SerializeField] Rigidbody2D rigidBody;
    [SerializeField] float jumpForce = 15.0f;

    private float jumpTimer = 0.0f;

    void Update()
    {
        if (Mathf.Approximately(jumpTimer, 0.0f)) {
            jumpTimer = Random.Range(2.0f, 5.0f);
        }

        jumpTimer -= Time.deltaTime;

        if (jumpTimer <= 0.0) {
            jumpTimer = 0.0f;
            Jump();
        }
    }

    private void Jump() {
        rigidBody.velocity = Vector2.up * jumpForce;
    }
}
