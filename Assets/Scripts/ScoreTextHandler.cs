﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreTextHandler : MonoBehaviour {

    [SerializeField] TextMeshProUGUI textMeshPro;

    private void Awake() {
        ScoresHandler.Instance.ScoreUpdateAction += UpdateText;
        ScoresHandler.Instance.UpdateScore();
    }

    private void OnDestroy() {
        if (ScoresHandler.Instance != null) {
            ScoresHandler.Instance.ScoreUpdateAction -= UpdateText;
        }
    }

    private void UpdateText(int scores) {
        textMeshPro.text = "SCORES: " + scores;
    }
}
