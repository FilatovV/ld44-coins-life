﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{
    [SerializeField] CameraFollow cameraFollow;
    [SerializeField] Player player;
    [SerializeField] RectTransform gameOverPanel;
    [SerializeField] WinPanel winPanel;
    [SerializeField] string nextLevelString;

    bool isGameOver = false;

    public Action RestartGameAction = delegate { };

    private void Start() {
        cameraFollow.Setup(CameraFollowFunction);
        player.GameOverAction = GameOver;
        ScoresHandler.Instance.UpdateScore();
    }

    private Vector3 CameraFollowFunction() {
        if (!isGameOver)
            return player.transform.position + new Vector3(player.IsLookToRight ? 1.0f : -1.0f, 1.0f, 0.0f);

        return Camera.main.transform.position;
    }

    private void GameOver() {
        isGameOver = true;
        gameOverPanel.gameObject.SetActive(true);
    }

    public void RestartLevel() {
        isGameOver = false;
        player.Restart();
        gameOverPanel.gameObject.SetActive(false);
        RestartGameAction();
    }

    public void FinishLevel() {
        if (string.IsNullOrEmpty(nextLevelString)) {
            Win();
            return;
        }
        ScoresHandler.Instance.SaveScore();
        SceneManager.LoadScene(nextLevelString);
    }

    public void Win() {
        isGameOver = true;
        winPanel.UpdateWinPanel();
        winPanel.gameObject.SetActive(true);
    }

    public void Exit() {
        SceneManager.LoadScene("StartScene");
    }
}
